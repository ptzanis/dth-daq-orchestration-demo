## DTH DAQ Orchestration Demo

# Receiver 
-  `docker compose -f receiver/docker/docker-compose.yml up --build -d`
- `curl -X POST -H "Content-Type: application/json" -d '{"command": "receiver/dth_recv/dth_recv > /dev/null 2>&1 &"}' http://127.0.0.1:7000/execute`

# Controller
- `sudo docker compose -f controller/docker/docker-compose.yml up --build -d`
- `curl -X POST -H "Content-Type: application/json" -d '{"command": "cd controller; python dthControl.py --function FPGA_sysmon"}' http://dth-p1-v2-06:7000/execute`
