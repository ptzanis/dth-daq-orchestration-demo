#!/usr/bin/python
import sys
import hal 
import pkg_resources
from dth.utilities import enableCard, findFile
from dth.dthlib import DTH
import argparse

ADDRESSTABLE_NAME = 'DTHAddressMap.dat'
PATHLIST = ['./',pkg_resources.resource_filename( 'dth', 'data/' ) ]

def main():
            
    adrtable = findFile( ADDRESSTABLE_NAME, PATHLIST )
    if not adrtable:
      print("Did not find addresstable in any of the following places: ", repr(PATHLIST) )
      sys.exit()    
    try:
      dth = DTH( adrtable, 0x10DC, 0x01B5, 0)
    except Exception:
          try :
            dth = DTH( adrtable, 0xECD6, 0xFEA1, 0)
          except Exception:
            print( "Didn't find any DTH board! You can try to reboot the comExpress on the DTH with the command: sudo reboot")
            sys.exit(-1)    
    parser = argparse.ArgumentParser(description='Simple script to run DTH P1 commands.')
    parser.add_argument('-func', '--function', choices=['FPGA_sysmon', 'loopmode_setup', 'loopmode_start', 'loopmode_stop'],
                        help='Choose a function to run')
    parser.add_argument('-extra', '--extra_functions', nargs='+', help='Run additional functions not in the choices list')

    args = parser.parse_args()

    if args.function:
        getattr(dth, args.function)()

    if args.extra_functions:
        for func_name in args.extra_functions:
            try:
                getattr(dth, func_name)()
            except AttributeError:
                print("Error: Function '{}' not found.".format(func_name))
     
if __name__ == "__main__":
	main()
