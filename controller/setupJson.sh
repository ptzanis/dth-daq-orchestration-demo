#!/bin/sh

echo "{\"DAQ_FEC\": $DAQ_FEC, \"sourceIp\": \"$sourceIp\", \"destIp\": \"$destIp\", \"sourcePort0\": $sourcePort0, \"destPort0\": $destPort0, \"sourcePort1\": $sourcePort1, \"destPort1\": $destPort1, \"FF_CDR\": $FF_CDR, \"SR0\": $SR0, \"SR1\": $SR1, \"FED_ID0\": $FED_ID0, \"FED_ID1\": $FED_ID1}" > DTH_control_parameters.json

echo "JSON file created!"