from flask import Flask, jsonify, request
import subprocess

app = Flask(__name__)

def execute_command(command):
    try:
        result = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT, universal_newlines=True)
        return result
    except subprocess.CalledProcessError as e:
        return f"Error: {e.output}"

@app.route('/execute', methods=['POST'])
def execute():
    data = request.get_json()
    if 'command' not in data:
        return jsonify({'error': 'Command not provided'}), 400

    command = data['command']
    result = execute_command(command)

    return jsonify({'result': result})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=7000)